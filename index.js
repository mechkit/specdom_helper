var HTML_tags = [
  'div',
  'span',
  'p',
  'a',
  'img',
  'ul',
  'li',
  'br',
  'h1',
  'h2',
  'h3',
  'input',
  'button',
  'select',
  'option',
  'table',
  'tr',
  'th',
  'td',
  'col',
  'em',
  'b',
  'mark',
  'strong',
  'cite',
  'i',
  'u',
  's',
  'q',
  'object',
];
var SVG_tags = [
  'svg',
  'SVGa',
  'animate',
  'animateMotion',
  'animateTransform',
  'circle',
  'clipPath',
  'ellipse',
  'defs',
  'desc',
  'discard',
  'g',
  'hatch',
  'hatchpath',
  'image',
  'line',
  'linearGradient',
  'marker',
  'mask',
  'metadata',
  'mpath',
  'path',
  'pattern',
  'polygon',
  'polyline',
  'radialGradient',
  'rect',
  'script',
  'set',
  'SVGstop',
  'style',
  'SVGswitch',
  'symbol',
  'text',
  'textPath',
  'title',
  'tspan',
  'use',
  'view',
];

var tags = Array.prototype.concat(HTML_tags, SVG_tags)

var tag_replace = {
  'SVGa': 'a',
  'SVGstop': 'stop',
  'SVGswitch': 'switch',
};


var mkSpec = function(tag, args){
  var props = undefined;
  var children = undefined;
  var meta = undefined;
  var onchange = undefined;


  args = Array.prototype.slice.call(args);
  var inputs = [];
  args.forEach(function(argument){
    if(argument){
      if(argument.constructor === Number){
        argument = String(argument);
      }
      if(argument.constructor === Function){
        onchange = argument;
      } else if(argument.constructor === Array){
        children = argument;
      } else if(argument.constructor === Object){
        if( props === undefined ){
          props = argument;
        } else {
          meta = argument;
        }
      } else if(argument.constructor === String){
        inputs.push(argument);
      } else {
        console.warn('what do I do with a',argument.constructor,'?');
      }
    }
  });

  props = props || {};
  if( onchange ){ props.onchange = onchange; }

  var spec = {
    tag: tag
  }
  if( props ){ spec.props = props; }
  if( meta ){ spec.meta = meta; }
  if( children ){ spec.children = children; }

  if( SVG_tags.indexOf(tag) !== -1 ){
    spec.namespaceURI = 'http://www.w3.org/2000/svg';
  }

  if( Object.keys(tag_replace).indexOf(tag) !== -1 ){
    tag = tag_replace[tag];
    spec.tag = tag;
  }

  if( inputs.length > 0 ){
    if( tags.indexOf(tag)+1 > 0 ){
      spec.text = inputs[0];
    }

    if( tag === 'a'){
      spec.props = spec.props || {};
      spec.props.href = spec.props.href || inputs[1];
    }
    if( tag === 'img'){
      spec.text = undefined;
      spec.props = spec.props || {};
      spec.props.src = spec.props.src || inputs[0];
      spec.props.alt = spec.props.alt || inputs[1];
    }
    if( tag === 'image'){
      spec.text = undefined;
      spec.props = spec.props || {};
      spec.props['xlink:href'] = spec.props.src || inputs[0];
    }
  }

  return spec;
};


var helper = {
  type: 'specDOM_helper'

  //div: function(){ return mkSpec('div', args); },
  //span: function(){ return mkSpec('span', args); },
  //a: function(){ return mkSpec('a', args); }

};


tags.forEach(function(tag){
  helper[tag] = function(){
    return mkSpec(tag, arguments);
  };
});

/**
 * .
 * @exports
 */
module.exports = helper;

//import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col, em, b, mark, strong, cite, i, u, s, q } from 'specdom_helper';
//import { div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col, em, b, mark, strong, cite, i, u, s, q, svg, animate, animateMotion, animateTransform, circle, clipPath, ellipse, defs, desc, discard, g, hatch, hatchpath, image, line, linearGradient, marker, mask, metadata, mpath, path, pattern, polygon, polyline, radialGradient, rect, script, set, SVGstop, style, SVGswitch, symbol, text, textPath, title, tspan, use, view } from 'specdom_helper';

