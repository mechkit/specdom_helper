# SpecDOM helper

```javascript
import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option, table, tr, th, td, col } from 'specdom_helper';
```

Each function takes multiple arguments and returns a [SpecDOM](https://gitlab.com/mechkit/specdom) object.

If an argument is an Object, is is added as 'props'.

If an argument is an Array, it is added as 'children'.

If an argument is a String, it is added as 'text'.

For the 'a' function, the first string is the 'text', the second is the 'href'.

## Example

```javascript
div({class:'title'},[
  span('hello world'),
  span(['goodbye world'])
])
```

Becomes:

```javascript
{
  tag: 'div',
  props: {
    class: 'title'
  },
  children: [
    {
      tag: 'span',
      text: 'hello world'
    },
    {
      tag: 'span',
      children: [
        'goodbye world'
      ]
    }
  ]
}
```

with [SpecDOM](https://gitlab.com/mechkit/specdom) becomes:

```html
<div class="title">
  <span>hello world</span>
  <span>goodbye world</span>
</div>
```
